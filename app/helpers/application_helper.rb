require 'mysql2'
module ApplicationHelper

  def query_master(sql)
    @db_host = DB_HOST
    @db_user = DB_USER
    @db_pass = DB_PASSWORD
    @db_name = "peeredge_#{current_subscriber_user.subscriber.downcase}"

    sanitized_sql_statement = sanitize_sql(sql)
    puts "..........................................................."
    puts ".....                 QUERY BROWSER                   ....."
    puts "..........................................................."
    puts ""
    puts "", sanitized_sql_statement.inspect
    puts ""
    puts "..........................................................."
    client = Mysql2::Client.new(:host => @db_host, :username => @db_user, :password => @db_pass, :database => @db_name)
    begin
      result = client.query(sanitized_sql_statement)
      client.close
    rescue => mysql_error
      puts "ERROR IN QUERY.....", mysql_error.inspect
      client.close
    end
    result
  end

  def sanitize_sql(sql)
    return_sql = sql.squish
    return return_sql
  end

end
