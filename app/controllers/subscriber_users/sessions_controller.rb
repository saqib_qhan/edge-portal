class SubscriberUsers::SessionsController < Devise::SessionsController
  skip_before_filter :check_agreement
  layout nil

  def new
    notice = flash[:notice]
    if notice.blank?
      notice = flash[:alert]
    end
    reset_session
    unless params[:id].blank?
      user = User.find(params[:id])
      unless user.blank?
        sign_in(:user, user)
        redirect_to "/dashboard", :notice => "Signed in successfully"
        return
      end
    else
      flash[:alert] = notice
      session[:url] = request.url
      resource = build_resource
      clean_up_passwords(resource)
      respond_with(resource, serialize_options(resource))
    end
  end

  def create

      resource = warden.authenticate!(auth_options)
      set_flash_message(:notice, :signed_in) if is_navigational_format?
      sign_in(resource_name, resource)
      respond_with resource, :location => after_sign_in_path_for(resource)
    #else
    #  flash[:notice] = "Invalid email or password"
    #  redirect_to '/subscriber'
    #end
  end


  def destroy
    redirect_path = after_sign_out_path_for(resource_name)
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :notice, :signed_out if signed_out

    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.any(*navigational_formats) { redirect_to redirect_path }
      format.all do
        method = "to_#{request_format}"
        text = {}.respond_to?(method) ? {}.send(method) : ""
        render :text => text, :status => :ok
      end
    end
  end

end
