class SubscriberUsers::RegistrationsController < Devise::RegistrationsController

  before_filter :authenticate_subscriber_user!

end