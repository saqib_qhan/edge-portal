class ApplicationController < ActionController::Base
  protect_from_forgery
  include ApplicationHelper


  def after_sign_out_path_for(resource_or_scope)
    home_path = "/subscriber"
    respond_to?(home_path, true) ? send(root_path) : home_path
  end

  def after_sign_in_path_for(resource_or_scope)
    #change_db
    #session[:current_user] = @current_user
    home_path = "/reportings/search_cdr"
    respond_to?(home_path, true) ? send(root_path) : home_path
  end

  def change_db
    unless current_subscriber_user.blank?
      ActiveRecord::Base.remove_connection
      a_spec = {
          :adapter => 'mysql2',
          :host => DB_HOST,
          :database => "peeredge_#{current_subscriber_user.subscriber.downcase}",
          :pool => 20,
          :username => DB_USER,
          :password => DB_PASSWORD
      }
      ActiveRecord::Base.establish_connection(a_spec)
    end
  end

  def change_to_parent_db
    ActiveRecord::Base.remove_connection
    a_spec = {
        :adapter => 'mysql2',
        :host => DB_HOST,
        :database => DATABASE_NAME,
        :pool => 20,
        :username => DB_USER,
        :password => DB_PASSWORD
    }
    ActiveRecord::Base.establish_connection(a_spec)
  end
end
