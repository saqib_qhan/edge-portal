require "csv"
require 'net/http'
require 'net/https'
require 'mysql2'
require 'dalli'
require "uri"
require 'rubygems'
require 'couchbase'
require 'active_record'
require 'date'
require 'fileutils'
#03234137353
class ReportingsController < ApplicationController
  include ApplicationHelper
  include ActionView::Helpers::NumberHelper
  before_filter :authenticate_subscriber_user!

  def search_cdr
    if session[:carrier_id].nil? and session[:current_user].nil?
      #change_db
      @subscriber_user = SubscriberUser.find(current_subscriber_user.id)
      #@carrier = Carrier.find(current_subscriber_user.carrier_id)
      if session[:current_user].nil?
        @current_user = current_subscriber_user
      end
      #session[:carrier_id] = @carrier.id
      #session[:carrier_name] = @carrier.name
      session[:current_user] = @current_user
    end
    @subscriber_user = session[:current_user]
    if @carrier.blank? and not session[:current_user].nil?
      @carrier = SubscriberUser.find(session[:current_user].id)
    end
    @lists        = []
    @current_user = session[:current_user]

  end

  def export_cdr
    begin
      @errors   = []
      file_sent = false
      ActiveRecord::Base.remove_connection
      parent_db = {
          :adapter  => 'mysql2',
          :host     => DB_HOST,
          :database => "peeredge_#{params[:subscriber].downcase}",
          :pool     => 20,
          :username => DB_USER,
          :password => DB_PASSWORD,
          :encoding => 'utf8'
      }
      ActiveRecord::Base.establish_connection(parent_db)

      date_start = params[:date_from].to_datetime
      date_end   = params[:date_to].to_datetime
      carrier    = Carrier.find_by_id(params[:selected_id])
      puts "Carriers ::::", carrier.name unless carrier.blank?
      unless carrier.blank?
        _directory = "#{Rails.root}/public/#{carrier.name}_#{Time.now.to_datetime.strftime("%Y%m%d%H%M%S")}"
        unless FileTest::directory?(_directory)
          Dir.mkdir(_directory, 0777)
        end
        zip = "#{_directory}/#{carrier.name}_#{date_start.to_date}_#{date_end.to_date}.zip"
        unless carrier.trunk_groups.blank?
          if params[:direction] == "originating"
            header = "ORIG CallID, FROM DID,TO DID,LRN,#{params[:direction] == "originating" ? "ORIG PREFIX" : "TERM PREFIX"},SIP CODE,SIP REASON,JURIS, #{params[:direction] == "originating" ? "ORIG TRUNK ID" : "TERM TRUNK ID"}, #{params[:direction] == "originating" ? "ORIG TG ID" : "TERM TG ID"},SRC IP, SWITCH IP, SETUPTIME,PDD,RAW BILL DURATION(sec),#{params[:direction] == "originating" ? "ORIG BILL SEC" : "TERM BILL SEC"},#{params[:direction] == "originating" ? "ORIG PRICE" : "TERM PRICE"},#{params[:direction] == "originating" ? "ORIG SURCHARGE" : "TERM SURCHARGE"},#{params[:direction] == "originating" ? "ORIG VALUE" : "TERM VALUE"},TIME,CREATED,#{params[:direction] == "originating" ? "ORIG CARRIER NAME" : "TERM CARRIER NAME"},#{params[:direction] == "originating" ? "ORIG TRUNK GROUP NAME" : "TERM TRUNK GROUP NAME"},CALL START TIME,CALL ANSWER TIME,CALL END TIME"
          else
            header = "FROM DID,TO DID,LRN,#{params[:direction] == "originating" ? "ORIG PREFIX" : "TERM PREFIX"},SIP CODE,SIP REASON,JURIS, #{params[:direction] == "originating" ? "ORIG TRUNK ID" : "TERM TRUNK ID"}, #{params[:direction] == "originating" ? "ORIG TG ID" : "TERM TG ID"}, SWITCH IP, SETUPTIME,PDD,RAW BILL DURATION(sec),#{params[:direction] == "originating" ? "ORIG BILL SEC" : "TERM BILL SEC"},#{params[:direction] == "originating" ? "ORIG PRICE" : "TERM PRICE"},#{params[:direction] == "originating" ? "ORIG SURCHARGE" : "TERM SURCHARGE"},#{params[:direction] == "originating" ? "ORIG VALUE" : "TERM VALUE"},TIME,CREATED,#{params[:direction] == "originating" ? "ORIG CARRIER NAME" : "TERM CARRIER NAME"},#{params[:direction] == "originating" ? "ORIG TRUNK GROUP NAME" : "TERM TRUNK GROUP NAME"},CALL START TIME,CALL ANSWER TIME,CALL END TIME"
          end
          carrier.trunk_groups.each do |truck_group|
            if truck_group.direction == params[:direction]
              file          = "#{_directory}/CDR_#{carrier.name}_#{truck_group.name}_#{date_start.to_date}_#{date_end.to_date}.csv"
              c             = Couchbase.connect(:bucket => 'production_cdr', :node_list => ['64.91.230.149:8091', '67.227.255.95:8091', '64.91.230.16:8091', '69.16.192.155:8091', '69.16.192.156:8091'])
              dumps         = c.design_docs['dumps2']
              batch_size    = 999999999999999999
              full_batch    = 1
              batch_counter = 0
              # Loop pulling docs until I don't get anything
              File.open(file, "w") do |csv|
                csv << header
                csv << "\n"
                while full_batch == 1
                  pulled_count = 0
                  dumps_fetch  = params[:direction] == "originating" ? dumps.byorigtg(:stale => 'ok', :limit => batch_size, :skip => batch_counter * batch_size, :include_docs => false, :startkey => ["#{params[:subscriber].downcase}", truck_group.id, date_start.year, date_start.month, date_start.day], :endkey => ["#{params[:subscriber].downcase}", truck_group.id, date_end.year, date_end.month, date_end.day]) : dumps.bytermtg(:stale => 'ok', :limit => batch_size, :skip => batch_counter * batch_size, :include_docs => false, :startkey => ["#{params[:subscriber].downcase}", truck_group.id, date_start.year, date_start.month, date_start.day], :endkey => ["#{params[:subscriber].downcase}", truck_group.id, date_end.year, date_end.month, date_end.day])
                  dumps_fetch.each do |cdr|
                    pulled_count = pulled_count + 1
                    created      = nil
                    setuptime    = nil
                    duration     = nil
                    cdr.value.split(",").each_with_index do |s, index|
                      if index == 12
                        setuptime = s
                      elsif index == 14
                        duration = s
                      elsif index == 20
                        created = s
                      end
                      if params[:direction] == "originating"
                        csv << s
                        csv << ","
                      else
                        if index == 0 or index == 10

                        else
                          csv << s
                          csv << ","
                        end
                      end

                    end

                    csv << carrier.name unless carrier.blank?
                    csv << ","
                    csv << truck_group.name unless truck_group.blank?
                    csv << ","
                    csv << created.to_datetime.strftime("%Y-%m-%d %H:%M:%S") unless created == "0000-00-00 00:00:00" or created == nil
                    csv << ","
                    csv << (created.to_datetime.to_time + setuptime.to_i).strftime("%Y-%m-%d %H:%M:%S") unless created == "0000-00-00 00:00:00" or created == nil
                    csv << ","
                    csv << (created.to_datetime.to_time + setuptime.to_i + duration.to_i).strftime("%Y-%m-%d %H:%M:%S") unless created == "0000-00-00 00:00:00" or created == nil
                    csv << "\n"
                  end
                  if pulled_count == batch_size
                    full_batch = 1
                  else
                    full_batch = 0
                  end
                end
              end
              file_sent = true
            end
          end
        end
        Zip::Archive.open(zip, Zip::CREATE) do |ar|
          Dir.glob("#{_directory}/*.csv").each do |path|
            ar.add_file(path.split('/').last, path)
          end
        end
      end
      change_to_parent_db
      if !file_sent
        @carrier = SubscriberUser.find(session[:current_user].id)
        respond_to do |format|
          format.html { redirect_to("/reportings/search_cdr",
                                    :notice => 'No Record Found.') }
        end
      else
        send_file(zip)
      end
    rescue => client
      change_to_parent_db
      puts "ERROR: ", client.inspect
      @carrier = SubscriberUser.find(session[:current_user].id)
      @errors << client
      render "search_cdr" if !file_sent
    end
  end

end