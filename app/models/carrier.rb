class Carrier < ActiveRecord::Base
  #has_many :carrier_contacts, :dependent => :destroy
  #has_many :carrier_phone_numbers, :dependent => :destroy
  has_many :trunk_groups, :dependent => :destroy
  #has_many :record_invoices, :dependent => :destroy
  #has_many :record_payments, :dependent => :destroy
  #has_one :carrier_balance_warning, :dependent => :destroy
  #belongs_to :user ,:foreign_key => "subscriber_id" ,:class_name => "User"
  belongs_to :user, :foreign_key => "subscriber_id", :class_name => "User"
end
